﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp
{
    class Solution
    {
        static List<Developer> developers = new List<Developer>();
        public DateTime jDate;

        static void Main(string[] args)
        {
            try
            {
                while (true)
                {
                    Console.WriteLine("Menu:");
                    Console.WriteLine("1. Create Developer");
                    Console.WriteLine("2. Calculate Total Salary/Charges");
                    Console.WriteLine("3. Exit");

                    Console.Write("Enter your choice: ");
                    string choiceStr = Console.ReadLine();
                    int choice;
                    if (!int.TryParse(choiceStr, out choice) || choice < 1 || choice > 3)
                    {
                        throw new Exception("Invalid choice.");
                    }

                    switch (choice)
                    {
                        case 1:
                            CreateDeveloper();
                            break;
                        case 2:
                            CalculateTotalSalaryOrCharges();
                            break;
                        case 3:
                            Environment.Exit(0);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        static void CreateDeveloper()
        {
            Console.Write("Enter developer ID: ");
            int id = Convert.ToInt32(Console.ReadLine());


            Console.Write("Enter developer name: ");
            string name = Console.ReadLine();

            Console.Write("Enter developer Assigned Project Name: ");
            string project = Console.ReadLine();

            DateOnly dateJoining = new DateOnly();

            Console.WriteLine("Choose type of developer:");
            Console.WriteLine("1. On Contract");
            Console.WriteLine("2. On Payroll");
            Console.Write("Enter your choice: ");
            int choice = Convert.ToInt32(Console.ReadLine());

            if (choice == 1)
            {

                Console.WriteLine("Enter duration in hours");
                int duration = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter Charges Per Day");
                double chargesPerDay = Convert.ToDouble(Console.ReadLine());

                developers.Add(new OnContract(id, name, dateJoining, project, duration, chargesPerDay));

            }

            else if (choice == 2)
            {
                Console.WriteLine("Enter Department : ");
                string department = Console.ReadLine();

                Console.WriteLine("Enter Manager name : ");
                string mName = Console.ReadLine();

                Console.WriteLine("Enter Basic Salary : ");
                double b_Salary = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Enter DA : ");
                double da = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Enter HRA : ");
                double hra = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Enter LTA : ");
                double lta = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Enter PF : ");
                double pf = Convert.ToDouble(Console.ReadLine());

                developers.Add(new OnPayroll(id, name, dateJoining, project, department, mName, b_Salary, da, hra, lta, pf));

            }

            else
            {

                Console.WriteLine("Invalid Choice");
                return;
            }

        }


        static void CalculateTotalSalaryOrCharges()
        {
            if (developers.Count == 0)
            {
                Console.WriteLine("No developers found.");
                Console.WriteLine();
                return;
            }

            Console.WriteLine("Choose calculation type:");
            Console.WriteLine("1.  Salary");
            Console.WriteLine("2.  Charges");

            Console.Write("Enter your choice: ");
            int choice = Convert.ToInt32(Console.ReadLine());


            double total = 0;
            if (choice == 1)
            {
                foreach (OnPayroll developer in developers.OfType<OnPayroll>())
                {
                    Console.WriteLine(developer.Id + "\t" + developer.Name + "\t" + developer.CalculateSalary());
                }
            }
            else
            {
                foreach (OnContract developer in developers.OfType<OnContract>())
                {
                    Console.WriteLine(developer.Id + "\t" + developer.Name + "\t" + developer.CalculateSalary());
                }
            }

            Console.WriteLine("Total: " + total);
            Console.WriteLine();
        }

        public static void LinqOperations()
        {
            // Display all records
            Console.WriteLine("All records:");
            foreach (Developer developer in developers)
            {
                Console.WriteLine(developer.ToString());
            }

            // Display all records where net salary is more than 20000
            Console.WriteLine("Records where net salary is more than 20000:");
            var PayRollDevelopers = developers.OfType<OnPayroll>().Where(developer => developer.CalculateSalary() > 20000);
            foreach (OnPayroll developer in PayRollDevelopers)
            {
                Console.WriteLine(developer.CalculateSalary());
            }

            // Display all records where name contains 'D'
            Console.WriteLine("Records where name contains 'D':");
            var D_Developers = developers.Where(developer => developer.Name.Contains("D"));
            foreach (Developer developer in D_Developers)
            {
                Console.WriteLine(developer.Name);
            }
        }

    }
}



