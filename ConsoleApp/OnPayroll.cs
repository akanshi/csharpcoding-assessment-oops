﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public class OnPayroll : Developer
    {
        public string Department { get; set; }
        public string Manager { get; set; }
        public double BasicSalary { get; set; }
        public double Da { get; set; }
        public double Hra { get; set; }
        public double Lta { get; set; }
        public double Pf { get; set; }

        public OnPayroll(int id, string name, DateOnly jDate, string projAssigned, string department, string manager, double basicSalary, double da, double hra, double lta, double pf) : base(id, name, jDate, projAssigned)
        {
            this.Department = department;
            this.Manager = manager;
            this.BasicSalary = basicSalary;
            this.Da = da;
            this.Hra = hra;
            this.Lta = lta;
            this.Pf = pf;
        }

        public override double CalculateSalary()
        {
            return BasicSalary + Da + Hra + Lta - Pf;
        }



    }
}
