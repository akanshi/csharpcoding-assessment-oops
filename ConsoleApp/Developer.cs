﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public abstract class Developer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateOnly JoiningDate { get; private set; }

        public string ProjectAssigned { get; set; }

        public abstract double CalculateSalary();

        public Developer(int id, string name, DateOnly jDate, string projAssigned)
        {
            this.Id = id;
            this.Name = name;
            this.JoiningDate = jDate;
            this.ProjectAssigned = projAssigned;
        }
    }

}
