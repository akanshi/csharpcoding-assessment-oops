﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public class OnContract : Developer
    {
        public int Duration { get; set; }
        public double ChargesPerDay { get; set; }

        public OnContract(int id, string name, DateOnly jDate, string projAssigned, int duration, double chargesPerDay) : base(id, name, jDate, projAssigned)
        {
            this.Duration = duration;
            this.ChargesPerDay = chargesPerDay;
        }

        public override double CalculateSalary()
        {
            return Duration * ChargesPerDay;
        }
    }
}
